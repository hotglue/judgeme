"""Stream type classes for tap-judgeme."""

from typing import Optional
from singer_sdk import typing as th

from tap_judgeme.client import JudgemeStream


class ReviewsStream(JudgemeStream):
    """Define custom stream."""
    name = "reviews"
    path = "/reviews"
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.reviews[*]"

    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("title", th.StringType),
        th.Property("body", th.StringType),
        th.Property("rating", th.NumberType),
        th.Property("product_external_id", th.NumberType),
        th.Property('reviewer',th.ObjectType(
            th.Property("id", th.NumberType),
            th.Property("external_id", th.NumberType),
            th.Property("email", th.StringType),
            th.Property("name", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("accepts_marketing", th.BooleanType),
            th.Property("unsubscribed_at", th.DateTimeType),
            th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        )),
        th.Property("source", th.StringType),
        th.Property("curated", th.StringType),
        th.Property("hidden", th.BooleanType),
        th.Property("verified", th.StringType),
        th.Property("featured", th.BooleanType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("ip_address", th.StringType),
        th.Property("pictures", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_title", th.StringType),
        th.Property("product_handle", th.StringType),
       
    ).to_dict()
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "rewviewer_id": record["reviewer"]["id"],
        }


class ReviewerStream(JudgemeStream):
    """Define custom stream."""
    name = "reviewers"
    path = "/reviewers/{rewviewer_id}"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ReviewsStream
    records_jsonpath = "$.reviewer[*]"

    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("email", th.StringType),
        th.Property("name", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        th.Property("accepts_marketing", th.BooleanType),
        th.Property("unsubscribed_at", th.DateTimeType),
        th.Property("external_id", th.NumberType),
        
    ).to_dict()

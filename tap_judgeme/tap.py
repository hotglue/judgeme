"""Judgeme tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_judgeme.streams import (
    JudgemeStream,
    ReviewsStream,
    ReviewerStream,
)
STREAM_TYPES = [
    ReviewsStream,
    ReviewerStream,
]

class TapJudgeme(Tap):
    """Judgeme tap class."""
    name = "tap-judgeme"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "shop_domain",th.StringType,required=True),  
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == '__main__':
    TapJudgeme.cli()        
